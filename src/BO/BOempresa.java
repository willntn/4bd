/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

/**
 *
 * @author willi
 */
public class BOempresa {
    int idempresa;
    String nomeEmpresa;
    BOgerente gerenteEmpresa;
    BOramoEmpresa ramoEmpresa;

    public int getIdempresa() {
        return idempresa;
    }

    public void setIdempresa(int idempresa) {
        this.idempresa = idempresa;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public BOgerente getGerenteEmpresa() {
        return gerenteEmpresa;
    }

    public void setGerenteEmpresa(BOgerente gerenteEmpresa) {
        this.gerenteEmpresa = gerenteEmpresa;
    }

    public BOramoEmpresa getRamoEmpresa() {
        return ramoEmpresa;
    }

    public void setRamoEmpresa(BOramoEmpresa ramoEmpresa) {
        this.ramoEmpresa = ramoEmpresa;
    }
}
