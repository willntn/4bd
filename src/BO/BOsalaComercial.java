/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

/**
 *
 * @author willi
 */
public class BOsalaComercial {
    int idSalaComercial;
    int numeroSalaComercial;

    public int getIdSalaComercial() {
        return idSalaComercial;
    }

    public void setIdSalaComercial(int idSalaComercial) {
        this.idSalaComercial = idSalaComercial;
    }

    public int getNumeroSalaComercial() {
        return numeroSalaComercial;
    }

    public void setNumeroSalaComercial(int numeroSalaComercial) {
        this.numeroSalaComercial = numeroSalaComercial;
    }
    
}
