/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

/**
 *
 * @author willi
 */
public class BOramoEmpresa {
    int idRamoEmpresa;
    String nomeRamoEmpresa;

    public int getIdRamoEmpresa() {
        return idRamoEmpresa;
    }

    public void setIdRamoEmpresa(int idRamoEmpresa) {
        this.idRamoEmpresa = idRamoEmpresa;
    }

    public String getNomeRamoEmpresa() {
        return nomeRamoEmpresa;
    }

    public void setNomeRamoEmpresa(String nomeRamoEmpresa) {
        this.nomeRamoEmpresa = nomeRamoEmpresa;
    }
}
