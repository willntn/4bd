/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UC;

import BO.*;
import DAO.*;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author willi
 */
public class UCall {
    
    
    
    public void insertData(BOempresa empresa, BOsalaComercial salaComercial) throws SQLException, ClassNotFoundException{
    
        Connection conexao = new conexaoMySQL().abrirConexao();
        conexao.setAutoCommit(false);
        
        DAOramoEmpresa dr = new DAOramoEmpresa();
        dr.cadastraRamoEmpresa(empresa.getRamoEmpresa(), conexao);
        
        DAOgerente dg = new DAOgerente();
        if(empresa.getGerenteEmpresa().getCpfGerente().length() != 11){
            System.out.println("cpf maior que permitido, transação não sera efetivada");
            conexao.rollback();
            return;            
        }
        dg.cadastraGerente(empresa.getGerenteEmpresa(), conexao);
        
        System.exit(1);
        DAOempresa de = new DAOempresa();
        de.cadastraEmpresa(empresa, conexao);
        
        DAOsalaComercial dsc = new DAOsalaComercial();
        dsc.cadastraSalaComercial(salaComercial, conexao);
        
        DAOsalaComercialHasEmpresa dse = new DAOsalaComercialHasEmpresa();
        dse.cadastraAssociativa(empresa, salaComercial);
        
        conexao.commit();
        conexao.close();
        
        System.out.println("Transação realizada com sucesso");
        
        
        
        
        
    }
}
