/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import BO.BOsalaComercial;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author willi
 */
public class DAOsalaComercial {
    
    public void cadastraSalaComercial(BOsalaComercial salaComercial, Connection conexao) throws SQLException{
    
     String comando = "insert into salaComercial(numero) values(?)";
        PreparedStatement stmt = conexao.prepareStatement(comando);
        
        stmt.setInt(1,salaComercial.getNumeroSalaComercial());        
        stmt.execute(); 
    
    }
    
}
