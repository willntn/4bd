/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author willi
 */
public class conexaoMySQL {
    
    static Connection conexao;   
    static String driverName = "com.mysql.jdbc.Driver";
    static String usuario = "root";
    static String senha = "root";
    static String serverName = "localhost";
    static String nomeDB = "salacomercialshopping";
    static String url =  url="jdbc:mysql://" + serverName + "/" + nomeDB;
    
    public conexaoMySQL(){}
  
    
    public  Connection abrirConexao() throws SQLException, ClassNotFoundException{            
            
        try{
            
            conexao = (Connection) DriverManager.getConnection(url,usuario,senha);
            Class.forName(driverName);
        }catch(ClassNotFoundException e){
            System.out.println("Driver nao encontrado: "+e.getMessage());
            
        }catch(SQLException e){
            System.out.println("Não foi possivel conectar ao bd: "+e.getMessage());
        }
        
        return conexao;
    }
    
    
    
    public  boolean fecharConexa() throws SQLException{
        try{
            conexao.close();
            
        }catch(SQLException e){
            System.out.println("Não foi possivel fechar: "+e.getMessage());
            return false;
        }
        
        return true;
    }
    
    
}
