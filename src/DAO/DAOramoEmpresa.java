/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import BO.BOramoEmpresa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author willi
 */
public class DAOramoEmpresa {
    
    public void cadastraRamoEmpresa(BOramoEmpresa ramoEmpresa, Connection conexao) throws SQLException, ClassNotFoundException{
        String comando = "insert into ramoEmpresa(nomeRamoEmpresa) values(?)";
        PreparedStatement stmt = conexao.prepareStatement(comando);
       
        stmt.setString(1,ramoEmpresa.getNomeRamoEmpresa());
        stmt.execute(); 
                
    }
    
    public int getCountRamoEmpresa(Connection conexao) throws SQLException{
        String comando = "select count(*) from ramoEmpresa";
        PreparedStatement stmt = conexao.prepareStatement(comando);
        ResultSet rs = stmt.executeQuery();
        rs.next();
        return rs.getInt("count(*)");
    }
    
}
