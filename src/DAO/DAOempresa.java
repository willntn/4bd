/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import BO.BOempresa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author willi
 */
public class DAOempresa {
    
    
    
    public void cadastraEmpresa(BOempresa empresa, Connection conexao) throws SQLException{
        String comando = "insert into empresa(nomeEmpresa,ramoEmpresa_idramoEmpresa, gerente_idgerente) values(?,?,?)";
        
        PreparedStatement stmt = conexao.prepareStatement(comando);
        
        stmt.setString(1,empresa.getNomeEmpresa());
        stmt.setInt(2,1);
        stmt.setInt(3,2);
        stmt.execute(); 
    }
    
    
    public int getCountEmpresa(Connection conexao) throws SQLException{
        String comando = "select count(*) from empresa";
        PreparedStatement stmt = conexao.prepareStatement(comando);
        ResultSet rs = stmt.executeQuery();
        rs.next();
        return rs.getInt("count(*)");
    }
}
