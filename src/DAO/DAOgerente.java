/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import BO.BOgerente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author willi
 */
public class DAOgerente {
    
    
    
    public void cadastraGerente(BOgerente gerente, Connection conexao) throws SQLException{
        String comando = "insert into gerente(nomeGerente,cpfGerente) values(?,?)";
        PreparedStatement stmt = conexao.prepareStatement(comando);
        
        stmt.setString(1,gerente.getNomeGerente());
        stmt.setString(2,gerente.getCpfGerente());
        stmt.execute(); 
    }
    
    public int getCountGerente(Connection conexao) throws SQLException{
        String comando = "select count(*) from gerente";
        PreparedStatement stmt = conexao.prepareStatement(comando);
        ResultSet rs = stmt.executeQuery();
        rs.next();
        return rs.getInt("count(*)");
    }
}
